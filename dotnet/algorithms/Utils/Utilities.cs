﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Algorithms.Utils
{
    public class Utilities
    {
        /// <summary>
        /// In debugging optimization algorithms with Random samplers it is crucial
        /// to have a controll over the initialization of every random generator
        /// and have the minimal necessary amount of such generators
        /// </summary>
        public static Random RandomGenerator = new Random(1);

        public static void SetRandomSeed(int seed)
        {
            RandomGenerator = new Random(seed);
        }

        public static double GenerateGaussian(double mean, double stdDev)
        {
            double u1 = 1.0 - RandomGenerator.NextDouble(); //uniform(0,1] random doubles
            double u2 = 1.0 - RandomGenerator.NextDouble();
            double randStdNormal = Math.Sqrt(-2.0 * Math.Log(u1)) *
                         Math.Sin(2.0 * Math.PI * u2); //random normal(0,1)
            return mean + stdDev * randStdNormal; //random normal(mean,stdDev^2)
        }

    }
}
