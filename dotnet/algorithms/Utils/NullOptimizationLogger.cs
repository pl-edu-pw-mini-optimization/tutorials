﻿using System;
using System.Collections.Generic;
using System.Text;
using Algorithms.Core;

namespace Algorithms.Utils
{
    class NullOptimizationLogger : IOptimizationLogger
    {
        public void FlushSamples()
        {
            //do nothing
        }

        public void LogSample(ValuedSample sample)
        {
            //do nothing
        }
    }
}
