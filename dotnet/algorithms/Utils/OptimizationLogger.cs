﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading;
using System.Linq;
using System.Globalization;
using Algorithms.Core;

namespace Algorithms.Utils
{
    public class OptimizationLogger : IOptimizationLogger
    {
        private const int SAMPLES_BUFFER = 1000;
        private List<ValuedSample> samplesToStore;
        private List<ValuedSample> samples;
        private object addingLock = new object();
        private object storingLock = new object();
        private bool isFirstSample;

        private string logName;
        private string loggedAlgorithmName;
        private DateTime loggingStartTime;

        public OptimizationLogger(string logName, string loggedAlgorithmName)
        {
            this.logName = logName;
            this.loggedAlgorithmName = loggedAlgorithmName;
            ResetLogger();
        }

        void ResetLogger()
        {
            loggingStartTime = DateTime.Now;
            lock (addingLock)
            {
                samples = new List<ValuedSample>();
            }
            lock (storingLock)
            {
                isFirstSample = true;
                File.Delete(logName);
                samplesToStore = new List<ValuedSample>();
            }
        }

        public void LogSample(ValuedSample sample)
        {
            lock (addingLock)
            {
                samples.Add(sample);
            }
            if (samples.Count >= SAMPLES_BUFFER)
            {
                FlushSamples();
            }
        }

        public void FlushSamples()
        {
            lock (addingLock)
            {
                lock (storingLock)
                {
                    samplesToStore.AddRange(samples);
                    Thread storingThread = new Thread(StoreSamples);
                    storingThread.Start();
                }
                samples.Clear();
            }
        }

        protected void StoreSamples()
        {
            lock (storingLock)
            {
                if (isFirstSample && samplesToStore.Any())
                {
                    File.AppendAllLines(logName, new string[] {
                        string.Format(
                            "Name\t" +
                            "Value\t{0}",
                            string.Join("\t", samplesToStore[0].X.Select((s,idx) => string.Format("X{0}", idx+1)))
                            )
                    });
                    isFirstSample = false;
                }
                IEnumerable<string> lines = samplesToStore.Select(sample =>
                    string.Format(CultureInfo.InvariantCulture, "{0}\t{1}\t{2}", loggedAlgorithmName, sample.Value,
                        string.Join("\t", sample.X.Select(x =>
                            string.Format(CultureInfo.InvariantCulture, "{0}", x)
                            ))
                        )
                    );
                File.AppendAllLines(logName, lines);
                samplesToStore.Clear();
            }
        }


    }
}
