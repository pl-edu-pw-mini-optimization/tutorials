﻿using Algorithms.Core;

namespace Algorithms.Utils
{
    public interface IOptimizationLogger
    {
        void FlushSamples();
        void LogSample(ValuedSample sample);
    }
}