using System;

namespace Algorithms.Functions
{
    public class RastriginFunction: IQualityFunction
    {
        public RastriginFunction(int dimension)
        {
            this.Dimension = dimension;
            this.LowerBoundary = new double[dimension];
            this.UpperBoundary = new double[dimension];
            for (int dim = 0; dim < dimension; dim++)
            {
                this.LowerBoundary[dim] = -5.12;
                this.UpperBoundary[dim] = 5.12;
            }
        }
        public double Value(double[] x)
        {
            if (this.Dimension != x.Length)
                throw new ArgumentException("Wrong argument size");
            double result = 0.0;
            const double A = 10.0; 
            for (int dim = 0; dim < Dimension; ++dim)
            {
                result += x[dim] * x[dim];
                result -= A * Math.Cos(2 * Math.PI * x[dim]);
            }
            result += A * Dimension;
            return result;
        }

        public int Dimension { get; private set; }

        public double[] LowerBoundary { get; private set; }

        public double[] UpperBoundary { get; private set; }
    }
}