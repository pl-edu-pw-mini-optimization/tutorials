using System;

namespace Algorithms.Functions
{
    public class RosenbrockFunction: IQualityFunction
    {
        public RosenbrockFunction(int dimension)
        {
            this.Dimension = dimension;
            this.LowerBoundary = new double[dimension];
            this.UpperBoundary = new double[dimension];
            for (int dim = 0; dim < dimension; dim++)
            {
                this.LowerBoundary[dim] = -2.048;
                this.UpperBoundary[dim] = 2.048;
            }
        }
        public double Value(double[] x)
        {
            if (this.Dimension != x.Length)
                throw new ArgumentException("Wrong argument size");
            double result = 0.0;
            for (int dim = 0; dim < Dimension - 1; ++dim)
            {
                result += 100 * (x[dim+1]-x[dim]*x[dim]) *
                    (x[dim + 1] - x[dim] * x[dim]);
                result += (1 - x[dim]) * (1 - x[dim]);
            }
            return result;
        }

        public int Dimension { get; private set; }

        public double[] LowerBoundary { get; private set; }

        public double[] UpperBoundary { get; private set; }
    }
}