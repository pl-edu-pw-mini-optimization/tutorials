﻿using System;

namespace Algorithms.Functions
{
    public interface IQualityFunction
    {
        double Value(double[] x);
        int Dimension { get; }

        double[] LowerBoundary { get; }

        double[] UpperBoundary { get; }
    }
}
