﻿using System;
using System.Collections.Generic;
using System.Text;
using Algorithms.Functions;
using Algorithms.Utils;

namespace Algorithms.Core
{
    public class HillClimbingAlgorithm : OptimizationAlgorithm
    {
        private readonly IOptimizationLogger logger;
        private readonly int failuresToReset;
        private readonly int maxSamplesCount;
        private readonly bool resetResetsFailuresCounter;
        private readonly double step;

        public HillClimbingAlgorithm(IQualityFunction functionToOptimize,
            IOptimizationLogger logger,
            int failuresToReset,
            int maxSamplesCount,
            double step,
            bool resetResetsFailuresCounter): base(functionToOptimize)
        {
            this.logger = logger;
            this.failuresToReset = failuresToReset;
            this.maxSamplesCount = maxSamplesCount;
            this.step = step;
            this.resetResetsFailuresCounter = resetResetsFailuresCounter;
        }

        public override ValuedSample Optimize()
        {
            int samplesCount = 1;
            int lastSuccess = 0;
            double[] x = RandomSampleWithinFunctionBounds();
            double[] xprime = new double[functionToOptimize.Dimension];
            ValuedSample bestSample = new ValuedSample(x, functionToOptimize);
            logger.LogSample(bestSample);
            while (samplesCount < maxSamplesCount)
            {
                x.CopyTo(xprime, 0);
                for (int dim = 0; dim < functionToOptimize.Dimension; ++dim)
                {
                    do
                    {
                        xprime[dim] = Utilities.GenerateGaussian(xprime[dim], step);
                    } while (xprime[dim] < functionToOptimize.LowerBoundary[dim] || xprime[dim] > functionToOptimize.UpperBoundary[dim]);
                }
                ValuedSample testSample = new ValuedSample(xprime, functionToOptimize);
                samplesCount++;
                logger.LogSample(testSample);
                if (testSample.Value < bestSample.Value)
                {
                    lastSuccess = 0;
                    bestSample = testSample;
                    xprime.CopyTo(x, 0);
                }
                else
                {
                    lastSuccess++;
                }
                if (lastSuccess > failuresToReset)
                {
                    if (resetResetsFailuresCounter)
                        lastSuccess = 0;
                    x = RandomSampleWithinFunctionBounds();
                }
            }
            logger.FlushSamples();
            return bestSample;
        }
    }
}
