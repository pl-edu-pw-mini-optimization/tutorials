﻿using System;
using System.Collections.Generic;
using System.Text;
using Algorithms.Functions;

namespace Algorithms.Core
{
    public class ValuedSample
    {
        public ValuedSample(double[] x, IQualityFunction function)
        {
            X = new double[x.Length];
            x.CopyTo(X, 0);
            Value = function.Value(x);
        }

        public double[] X { get; private set; }
        public double Value { get; private set; }
    }
}
