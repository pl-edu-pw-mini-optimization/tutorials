﻿using System;
using System.Collections.Generic;
using System.Text;
using Algorithms.Functions;
using Algorithms.Utils;

namespace Algorithms.Core
{
    public abstract class OptimizationAlgorithm
    {
        protected readonly IQualityFunction functionToOptimize;

        public OptimizationAlgorithm(IQualityFunction functionToOptimize)
        {
            this.functionToOptimize = functionToOptimize;
        }
        protected double[] RandomSampleWithinFunctionBounds()
        {
            double[] x = new double[functionToOptimize.Dimension];
            for (int dim = 0; dim < functionToOptimize.Dimension; dim++)
            {
                x[dim] = functionToOptimize.LowerBoundary[dim] +
                    (functionToOptimize.UpperBoundary[dim] - functionToOptimize.LowerBoundary[dim]) * Utilities.RandomGenerator.NextDouble();
            }

            return x;
        }

        public abstract ValuedSample Optimize();

    }
}
