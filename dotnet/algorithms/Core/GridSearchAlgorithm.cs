﻿using System;
using System.Collections.Generic;
using System.Text;
using Algorithms.Functions;
using Algorithms.Utils;

namespace Algorithms.Core
{
    public class GridSearchAlgorithm : OptimizationAlgorithm
    {
        private readonly IOptimizationLogger logger;
        private readonly int gridDensityPerDimension;

        public GridSearchAlgorithm(IQualityFunction functionToOptimize,
            IOptimizationLogger logger,
            int gridDensityPerDimension) : base(functionToOptimize)
        {
            this.logger = logger;
            this.gridDensityPerDimension = gridDensityPerDimension;
        }

        public override ValuedSample Optimize()
        {
            OptimizationAlgorithm initialOptimizer = new MonteCarloAlgorithm(this.functionToOptimize,
                new NullOptimizationLogger(),
                1000);
            ValuedSample startOptimum = initialOptimizer.Optimize();
            double[] x = new double[functionToOptimize.Dimension];
            startOptimum.X.CopyTo(x, 0);
            ValuedSample bestOptimumEstimation = new ValuedSample(x, functionToOptimize);
            for (int dim1 = 0; dim1 < functionToOptimize.Dimension; ++dim1)
            {
                for (int dim2 = dim1 + 1; dim2 < functionToOptimize.Dimension; ++dim2)
                {
                    for (double stepDim1 = 0; stepDim1 < gridDensityPerDimension; ++stepDim1)
                    {
                        x[dim1] = functionToOptimize.LowerBoundary[dim1] +
                            (functionToOptimize.UpperBoundary[dim1] - functionToOptimize.LowerBoundary[dim1]) *
                            (stepDim1 / (gridDensityPerDimension - 1));
                        for (double stepDim2 = 0; stepDim2 < gridDensityPerDimension; ++stepDim2)
                        {
                            x[dim2] = functionToOptimize.LowerBoundary[dim2] +
                                (functionToOptimize.UpperBoundary[dim2] - functionToOptimize.LowerBoundary[dim2]) *
                                (stepDim2 / (gridDensityPerDimension - 1));
                            ValuedSample testSample = new ValuedSample(x, functionToOptimize);
                            logger.LogSample(testSample);
                            if (testSample.Value < bestOptimumEstimation.Value)
                            {
                                bestOptimumEstimation = testSample;
                            }
                        }
                    }
                    x = new double[functionToOptimize.Dimension];
                    bestOptimumEstimation.X.CopyTo(x, 0);
                }
            }
            logger.FlushSamples();
            return bestOptimumEstimation;
        }
    }
}
