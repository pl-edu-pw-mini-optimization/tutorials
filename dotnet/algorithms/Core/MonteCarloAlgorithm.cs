﻿using System;
using System.Collections.Generic;
using System.Text;
using Algorithms.Functions;
using Algorithms.Utils;

namespace Algorithms.Core
{
    public class MonteCarloAlgorithm : OptimizationAlgorithm
    {
        private readonly IOptimizationLogger logger;
        private readonly int sampleCount;

        public MonteCarloAlgorithm(IQualityFunction functionToOptimize,
            IOptimizationLogger logger,
            int sampleCount) : base(functionToOptimize)
        {
            this.logger = logger;
            this.sampleCount = sampleCount;
        }


        public override ValuedSample Optimize()
        {
            ValuedSample bestSample = null;
            for (int sampleIdx = 0; sampleIdx < sampleCount; sampleIdx++)
            {
                double[] x = RandomSampleWithinFunctionBounds();
                ValuedSample testSample = new ValuedSample(x, functionToOptimize);
                logger.LogSample(testSample);
                if (bestSample == null || testSample.Value < bestSample.Value)
                {
                    bestSample = testSample;
                }
            }
            logger.FlushSamples();
            return bestSample;
        }
    }
}
