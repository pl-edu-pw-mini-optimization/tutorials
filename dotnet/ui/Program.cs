﻿using Algorithms;
using System;
using Algorithms.Functions;
using Algorithms.Core;
using Algorithms.Utils;

namespace UI
{
    class Program
    {
        static void Main(string[] args)
        {
            OptimizationLogger logger = new OptimizationLogger("hc2-rastrigin-log-10000.csv", "HC");
            Utilities.SetRandomSeed(seed: 1);
            IQualityFunction qualityFunction = new RastriginFunction(dimension: 2);
            OptimizationAlgorithm algorithm =
                new HillClimbingAlgorithm(qualityFunction, logger,
                failuresToReset: 100, maxSamplesCount: 10000,
                step: 0.01, resetResetsFailuresCounter: false);
            ValuedSample result = algorithm.Optimize();
            Console.WriteLine(result.Value);
            Console.WriteLine(String.Join(';', result.X));
        }
    }
}
