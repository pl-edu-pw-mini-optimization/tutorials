# Tutorials

Sample .NET Core 3.0/.NET Standard 2.0 code for generating optimization algorithm results
and R scripts for post-processing algorithms performance data (convergence and 2D sampling plots).
Description is given in [wiki pages](../../wiki/Introduction%20to%20algorithm%20diagnostics).
